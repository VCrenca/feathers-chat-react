import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import client from '../../feathers'; 

export const findMessages = createAsyncThunk(
    'chat/FIND_MESSAGES',
    async () => {
        const response = await client.service('messages').find(
            {
                query: {
                    $sort: {sentAt: -1},
                    $limit: 15
                }
            }
        );
        return response.data.reverse(); 
    }
);


export const chatSlice = createSlice({
    name:"chat",
    initialState:{
        users: [],
        messages: []
    },
    reducers:{
        ADD_MESSAGE: (state, action) => {
            const { message } = action.payload;
            state.messages.push(message); 
        }, 
        CLEAR_ALL_MESSAGES: (state, action) => {
            state.messages.splice(0, state.messages.length); 
        }
    },
    extraReducers:{
        [findMessages.fulfilled]: (state, action) => {
            action.payload.forEach((message) => state.messages.push(message)); 
        }
    }
})

export const selectMessages = state => state.chat.messages;
export const selectUsers = state => state.chat.users;

export const {
    ADD_MESSAGE,
    CLEAR_ALL_MESSAGES
} = chatSlice.actions; 

export default chatSlice.reducer; 