import { createSlice } from '@reduxjs/toolkit';

export const authSlice = createSlice({
    name:"auth",
    initialState: {
        isLoggedIn: null,
        currentUser: null
    },
    reducers:{
        LOGOUT: (state) => {
            state.isLoggedIn = false;
            state.currentUser = null;
        },
        LOGIN: (state, action) => {
            const { payload : {user}} = action;
            state.isLoggedIn = true;
            state.currentUser = user; 
        },
        UPDATE_CURRENT_USER: (state, action) => {
            const { payload } = action;
            state.currentUser = payload
        }
    }
});

export const selectIsLoggedIn = state => state.auth.isLoggedIn;
export const selectCurrentUser = state => state.auth.currentUser;

export const {
    LOGOUT,
    LOGIN,
    UPDATE_CURRENT_USER
} = authSlice.actions;

export default authSlice.reducer;