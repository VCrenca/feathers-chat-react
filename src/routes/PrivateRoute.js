import React from 'react';
import {Route, Redirect} from 'react-router-dom'; 
import { useSelector } from 'react-redux';
import { selectIsLoggedIn } from '../features/auth/authSlice'; 

const PrivateRoute = ({component: Component, ...rest}) => {

    const isAuthenticated = useSelector(selectIsLoggedIn); 

    return(
        <Route
        {...rest}
        render={() => { 
            return isAuthenticated ? <Component {...rest}/> : isAuthenticated === null ? null : <Redirect to="/login"/>
        }}/>
    );
};

export default PrivateRoute;