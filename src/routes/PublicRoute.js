import React from 'react';
import { useSelector } from 'react-redux';
import { selectIsLoggedIn } from '../features/auth/authSlice'; 
import {Route, Redirect} from 'react-router-dom'; 


const PublicRoute = ({component: Component, restricted, ...props}) => {

    const isAuthenticated = useSelector(selectIsLoggedIn); 

    return(
        <Route
        {...props}
        render={(props) => (
            !isAuthenticated && restricted ?
            (<Component {...props}/>)
            : (<Redirect to="/"/>)
        )}/>
    );
};

export default PublicRoute;