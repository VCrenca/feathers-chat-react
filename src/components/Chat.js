import React, { useEffect, useState} from 'react';
import {useSelector, useDispatch} from "react-redux"; 
import { selectIsLoggedIn, selectCurrentUser } from '../features/auth/authSlice';
import { findMessages, selectMessages, ADD_MESSAGE, CLEAR_ALL_MESSAGES } from '../features/chat/chatSlice';  
import Message from './Message';   
import Users from './Users'; 
import client from '../feathers'; 

const Chat = () => {

    const [newMessage, setNewMessage] = useState('');
    const [currentReceiver, setCurrentReceiver] = useState(undefined); 

    const isLoggedIn = useSelector(selectIsLoggedIn); 
    const messages = useSelector(selectMessages); 
    const currentUser = useSelector(selectCurrentUser); 

    const dispatch = useDispatch();

    const updateMessage = (ev) => { 
        setNewMessage(ev.target.value); 
    };

    const sendMessage = (ev) => {
        ev.preventDefault();
        const input = document.querySelector('#input-to-send');
        return client.service('messages').create({
            text: newMessage   
        }).then(() => {
            input.value = '';
            setNewMessage(''); 
        }); 
    };

    useEffect(() => {
        const messagesContainer = document.querySelector('.messages-container');
        client.service('messages').on('created', (message) => {
            dispatch(ADD_MESSAGE({message}));
            messagesContainer.scrollTop = messagesContainer.scrollHeight - messagesContainer.clientHeight;
        }); 
        client.on('logout', () => {
            dispatch(CLEAR_ALL_MESSAGES()); 
        });
    }, [dispatch]); 

    useEffect(() => {
        if(isLoggedIn){
            dispatch(findMessages());
        }
    }, [isLoggedIn, dispatch]);

    useEffect(() => {
        if(messages.length !== 0){
            const messagesContainer = document.querySelector('.messages-container');
            messagesContainer.scrollTop = messagesContainer.scrollHeight - messagesContainer.clientHeight;
        }   
    }, [messages.length]);



    return(
        <div className="row h-100">
            <div className="col-sm-3">
                <h3 className="font-weight-light">Messages</h3>
            </div>
            <div className="messages d-flex flex-column flex-grow-1 h-100 border-left col-sm-9">
                <div className="messages-container d-flex flex-column flex-grow-1 overflow-auto pr-4">
                    {messages.map(message => <Message key={message._id} message={message}/>)}
                </div>
                <form className="d-flex" onSubmit={ev => sendMessage(ev)}>
                    <div className="form-group mb-2 flex-grow-1">
                        <input className="form-control" id="input-to-send" placeholder="Say something..." name="message" type="text" onChange={ev => updateMessage(ev)}/>
                    </div>
                    <div className="text-center send-button">
                        <button className="btn btn-primary mb-2 ml-2 send-button" type="submit" disabled={newMessage === ''}>Send</button>
                    </div>
                </form>
            </div>
        </div>
    );         
}; 

export default Chat; 