import React, {useState, useEffect, useCallback} from 'react';
import { selectCurrentUser, UPDATE_CURRENT_USER } from '../features/auth/authSlice'; 
import client from '../feathers'; 
import { useSelector, useDispatch } from 'react-redux';
 
const Users = () => {

    const [searchValue, setSearchValue] = useState(''); 
    const [searchResult, setSearchResult] = useState([]); 
    const [launchSearch, setLaunchSearch] = useState(false); 
    const [updateCurrentUser, setUpdateCurrentUser] = useState(false); 

    const currentUser = useSelector(selectCurrentUser); 
    
    const dispatch = useDispatch(); 

    const updateField = (ev) => {
        const {target} = ev;
        setSearchValue(target.value); 
    };

    const search = useCallback(() => {
        client.service('users').find({
            query:{
                $search: searchValue
            }}
        ).then((result) => {
            setSearchResult(result.data); 
        }); 
    }, [searchValue]);
    
    
    const doUpdateCurrentUser = useCallback(() => {
        client.service('users').get(currentUser._id)
        .then((user) => {
            dispatch(UPDATE_CURRENT_USER(user)); 
        });
    }, [currentUser, dispatch]);


    useEffect(() => {
        if(updateCurrentUser){
            doUpdateCurrentUser();
            setUpdateCurrentUser(false);
        }
    }, [updateCurrentUser, doUpdateCurrentUser]);


    useEffect(() => {
        if(launchSearch){
            search();
            setLaunchSearch(false); 
        }
    }, [launchSearch, search])


    useEffect(() => {
        client.service('users').on('followed', () => { 
            setLaunchSearch(true); 
            setUpdateCurrentUser(true);
        }); 
        client.service('users').on('unfollow', () => {
            setLaunchSearch(true); 
            setUpdateCurrentUser(true); 
        });
    }, []);

    const emptyField = () => {
        const input = document.querySelector('#search-input');
        input.value = ''; 
    };

    const enterTrigger = (ev) => {
        if(ev.keyCode === 13){
            search(); 
        } 
    };

    const follow = (result) => {
        client.service('users').patch(currentUser._id, {newFollow: result._id})
        .then((result) => {
            dispatch(UPDATE_CURRENT_USER(result));
        }); 
    };

    const unfollow = (result) => {
        client.service('users').patch(currentUser._id, {unfollow: result._id})
        .then((result) => dispatch(UPDATE_CURRENT_USER(result))); 
    }

    return(
        <div className='row'>
            <div className="col-md-6">
                <div className="input-group mb-4">
                    <input 
                    id="search-input" 
                    type="text" 
                    className="form-control"
                    name="search" 
                    placeholder="Search..." 
                    onChange={ev => updateField(ev)} 
                    onKeyDown={(ev) => enterTrigger(ev)} 
                    onBlur={emptyField}/>
                    <div className='input-group-prepend'>
                        <button onClick={search} class="btn btn-outline-secondary" type="button">Button</button>
                    </div>
                </div>
                {searchResult.map(result => (
                    <React.Fragment key={result._id}>
                        <div className="d-flex mb-3">
                            <img className="img-thumbnail search-avatar" src={result.avatar} alt={result.name}></img>
                            <div className="flex-grow-1 p-2 pl-3">
                                <span className="lead">{result.name}</span>
                                <div className="result-stats">
                                    <span>Followers : {result.followers.length}</span>
                                </div>
                            </div>
                            {currentUser._id === result._id ? null 
                            : currentUser.follows.indexOf(result._id) !== -1 ?                            
                                <button className="btn align-self-center btn-sm btn-outline-secondary" onClick={() => unfollow(result)}>-</button>
                            : 
                            <button className="btn align-self-center btn-sm btn-outline-secondary"  onClick={() => follow(result)}>+</button>
                            }
                        </div>
                    </React.Fragment>
                ))}
            </div>
        </div>
    );
};

export default Users;