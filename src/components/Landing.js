import React from 'react';
import { Link } from 'react-router-dom';

const Landing = () => {
    return(
        <div className='landing'>
            <h2>Welcome to Chat App</h2>
            <div className="landing-buttons">
                <Link to="/login"><button>Login</button></Link>
                <Link to="/sign_up"><button>Sign Up</button></Link>
            </div>
        </div>
    );
};

export default Landing; 