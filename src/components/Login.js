import React, { useState } from 'react';
import client from '../feathers'; 

const Login = () => {

    const [formState, setFormState] = useState({}); 

    const updateField = (ev) => {
        const {target} = ev;
        const {target : {name}} = ev; 
        setFormState(state => {
            state[name] = target.value;
            return state; 
        });
    };

    const login = (ev) => {
        ev.preventDefault(); 
        return client.authenticate({
            strategy: "local",
            email: formState["email"],
            password: formState["password"]
        }).catch((error) => console.log(error));
    };

    return(
        <div className="container">
            <div className="row pt-5">
                <div className="col-md-8">
                    <form className="login-form" onSubmit={(ev) => login(ev)}>
                        <h3 className="mb-3">Sign in</h3>
                        <div className="form-group">
                            <input type="email" className="form-control" name="email" autoComplete="username" placeholder="Email" onChange={ev => updateField(ev)}/>
                        </div>
                        <div className="form-group">
                            <input type="password" className="form-control" name="password" placeholder="Password" autoComplete="current-password" onChange={ev => updateField(ev)}/>
                        </div>
                        <div className="row">
                            <div className="col-12 d-flex justify-content-end">
                                <button className="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div className="col-md-4 d-flex flex-column align-items-center justify-content-center">
                    <a className="btn btn-secondary p-3" href="http://localhost:3030/oauth/google">Google</a>
                </div>
            </div>
        </div>
    );
};

export default Login;