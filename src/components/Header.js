import React from 'react';
import client from '../feathers'; 
import { useSelector, useDispatch } from 'react-redux'; 
import { selectIsLoggedIn, LOGOUT } from '../features/auth/authSlice'; 

const Header = () => {

    const isLoggedIn = useSelector(selectIsLoggedIn);
    const dispatch = useDispatch(); 

    const handleLogout = () => {
        client.logout().then(() => dispatch(LOGOUT()));  
      };

    return(
        <div className="p-3">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-10">
                        <div>
                            <h1 className="h2">Chat App</h1>
                            <span className="lead small">Powered by FeatherJS and Bootstrap</span>
                        </div>
                    </div>
                    <div className="col-2 justify-content-end d-flex align-items-center">
                        <div>
                            {isLoggedIn ? 
                            <button className="btn btn-secondary" onClick={handleLogout}>Logout</button> 
                            : null}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Header;