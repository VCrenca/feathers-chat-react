import React from 'react';
import { selectCurrentUser } from '../features/auth/authSlice';
import { useSelector } from 'react-redux'; 
import moment from 'moment'; 

const Message = ({message}) => {
    const currentUser = useSelector(selectCurrentUser);

    const isMyMessage = (message) => {
        if(message.sender._id === currentUser._id){
            return "align-self-end my-message";
        } else {
            return "align-self-start others-message";
        }
    };

    return(
        <div className={`mb-4 message ${isMyMessage(message)}`}>
            <div className={`card flex-shrink-0 border-0`}>
                <div className="d-flex align-items-center">
                    <img src={message.sender.avatar} alt="coucou" className="img-thumbnail mr-1 message-avatar align-self-end"/>
                    <div className="card-body text-break rounded text-light">
                        {message.text}
                    </div>
                </div>
            </div>
            <div className="text-right border-0 p-1">{moment(message.sentAt).format('LT')}</div>
        </div>
    );
};

export default Message;
