import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'; 
import { Route, Switch } from 'react-router-dom'; 
import './App.scss';
import client from './feathers'; 
import { LOGOUT, LOGIN } from './features/auth/authSlice';
import Login from './components/Login';
import Chat from './components/Chat';
import Users from './components/Users'; 
import Landing from './components/Landing';
import Header from './components/Header';  
import PublicRoute from './routes/PublicRoute';
import PrivateRoute from './routes/PrivateRoute'; 
import NoMatch from './routes/NoMatch';

function App() {

  const dispatch = useDispatch(); 

  useEffect(() => {
    const messageService = client.service('messages');
    const userService = client.service('users');
        
    client.reAuthenticate()
    .catch(() => {
      dispatch(LOGOUT()); 
    });

    client.on('authenticated', (login) => {
      dispatch(LOGIN({user: login.user}));
    });

    client.service('users').on('newFollow', (data) => {
      console.log(`New follow from ${data.from.name}`); 
    });

  }, [dispatch]);


  return (
    <div className="App vh-100">
      <Header/>
      <div className="container-fluid content p-4">
        <Switch>
          <PublicRoute restricted exact path="/" component={Landing}/>
          <PublicRoute path="/login" component={Login} restricted/>
          <PrivateRoute exact path="/chat" component={Chat}/>
          <PrivateRoute exact path="/search" component={Users}/>
          <Route path="*" component={NoMatch}/>
        </Switch>
      </div>  
    </div>
  );
}

export default App;
